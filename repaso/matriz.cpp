#include <stdio.h>
#include <stdlib.h>

#define X 3
#define Y 3

int main(){
    system("clear");
    /*Zona variables*/
    double op1,
           op2,
           op3,
           determinante;
    int tamano;

    /*Matriz*/
    double a[X][Y]; /*{*/
           /*0  1  2*/
    /*0*/ //{7, 3, 1},
    /*1*/ //{4, 2, 5},
    /*2*/ //{3, 1, 7}
    //};

    /*Pedida de datos*/
    printf("Tamaño del vector: ");
    scanf(" %i", &tamano);
    for (int i=0; i<3; i++){
        printf("Vector %i: ", i + 1);
        scanf(" %lf, %lf, %lf", &a[i][0], &a[i][1], &a[i][2]);
    }

    /*Operacion diagonal*/
    op1 = a[0][0] * a[1][1] * a[2][2];
    op2 = a[1][0] * a[2][1] * a[0][2];
    op3 = a[0][1] * a[1][2] * a[2][0];
    determinante = op1 + op2 + op3;


    /*Salida de datos*/
    printf("op1: %4.2lf\n", op1);
    printf("op2: %4.2lf\n", op2);
    printf("op3: %4.2lf\n", op3);

    /*Final*/
    printf("Determinante: %4.2lf\n", determinante);

    return EXIT_SUCCESS;
}
