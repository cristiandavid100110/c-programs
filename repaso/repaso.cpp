#include <stdio.h>
#include <stdlib.h>

#define N 1000

int main(){

    system("clear");
    int n_alumnos = 0;
    double  nota [N],
            entrada,
            media = 0;

    do{
        printf("Nota: ");
        scanf(" %lf", &entrada);
        if (entrada >=0)
            nota[n_alumnos++] = entrada;
    }while (entrada >=0);

    for (int i=0; i<n_alumnos; i++)
        media += nota[i];
    media /= n_alumnos;

    printf("Media: %lf\n",media);

	return EXIT_SUCCESS;
}
