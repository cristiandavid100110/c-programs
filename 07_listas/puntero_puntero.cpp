#include <stdio.h>
#include <stdlib.h>

int main(){
	unsigned numeros[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
	unsigned *tamano = numeros;
	unsigned **contenedor = &numeros;
	unsigned (*contenedor1)[9] = &numeros;

	printf("contenedor tiene  %p\n", contenedor);
	printf("tamaño tiene      %p\n", *contenedor);
	printf("numeros[0] tiene  %u\n", **contenedor);

	return EXIT_SUCCESS;
}
