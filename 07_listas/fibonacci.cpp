#include <stdio.h>
#include <stdlib.h>

#define N 30

int main(){
	system("clear");
	int num[N];

	num[1] = num[0] = 1;

	for(int i=2; i<=N; i++)
		num[i] = num[i-1] + num[i-2];

	for(int i=0; i<=N; i++){
		printf("%i ", num[i]);
	}

	printf("\n");

	for (int i=1; i<N; i++)
		printf("%.4lf ", (double)num[i] / num[i-1]);	

	printf("\n");
	
	return EXIT_SUCCESS;
}
