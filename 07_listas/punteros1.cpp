#include <stdio.h>
#include <stdlib.h>

int main(){
	system("clear");
	char letra [] = {'a', 'b', 'c'};

	printf("Esto es una letra %c\n", letra[0]);
	printf("Esto es otra letra %c\n", *(letra+1));
	printf("Esto es otra letra %c\n",*(letra+2));

	printf("\n");

	printf("Ocupa %i bytes\n", (int)sizeof(letra));
	printf("Ocupa %i bytes\n", (int)sizeof(letra[0]));

	return EXIT_SUCCESS;
}
