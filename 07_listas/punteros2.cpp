#include <stdio.h>
#include <stdlib.h>

int main(){
	system("clear");
	unsigned numeros[] = {1, 2, 3};
	char letra[] = {'a', 'b', 'c'};

	printf( "numero %u - "
		"letra %c\n"
		"numero %u - "
		"letra %c\n"
		"numero %u - "
		"letra %c\n",
		numeros[0],
		letra[0],
		numeros[1],
		letra[1],
		numeros[2],
		letra[2]
			);
	printf("soy la variable numeros y ocupo %i bytes\n", (int)sizeof(numeros));
	printf("soy la variable letra y ocupo %i bytes\n", (int)sizeof(letra));

	return EXIT_SUCCESS;
}
