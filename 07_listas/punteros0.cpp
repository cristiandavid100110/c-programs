#include <stdio.h>
#include <stdlib.h>

/* Reservando en memoria una variable a la que llamamos "numeros" sin signo.
 * En esta variable hemos introducido numeros del 0 al 9.
 * Si queremos el numero 5 solo tenemos que saber en que celda esta el 5.
 * Se puede hacer con printf seguido de la celda de memoria.
 * EJ:  numero [6] o con *(numero+6)
 * */

int main(){
	system("clear");
	unsigned numeros[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

	printf("Sin aritmética de punteros: %u y %u\n", numeros[0], numeros[9]);
	printf("Con aritmética de punteros: %u y %u\n", *numeros, *(numeros+9));

	printf("\n");

	return EXIT_SUCCESS;
}
