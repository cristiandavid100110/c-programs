#include <stdio.h>
#include <stdlib.h>

#define ROJO "\x1B[31m"
#define VERDE "\x1B[32m"
#define AZUL "\x1B[34m"
#define NEGRO "\x1B[30m"
#define BLANCO "\x1B[37m"
#define MAGENTA "\x1B[35m"
#define CYAN "\x1B[36m"
#define AMARILLO "\x1B[33m"


int main(){
    int  r, g, b;

    system("clear");

    printf("Estas viendo rojo?\n");
    scanf(" %i", &r);

    printf("Estoy viendo verde?\n");
    scanf(" %i", &g);

    printf("Estoy viendo azul?\n");
    scanf(" %i", &b);

    if (r)
        if (g)
            if (b)
                printf(BLANCO "Blanco\n");
            else
                printf(AMARILLO "amarillo\n");
        else
            if (b)
                printf(MAGENTA "magenta\n");
            else
                printf(ROJO "Rojo\n");
    else
        if (g)
            if (b)
                printf(CYAN "cyan\n");
            else
                printf(VERDE "verde\n");
        else
            if (b)
                printf(AZUL "Azul\n");
            else
                printf(NEGRO "Negro\n");

    return EXIT_SUCCESS;
}
