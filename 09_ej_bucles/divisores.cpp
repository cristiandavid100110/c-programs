#include <stdio.h>
#include <stdlib.h>

#define N 25
#define M 30

/* Función punto de entrada */
int  main(){
    int div[N];
    int div1[M];
    int num = 25, n_div=0;
    int num1 = 30, n_div1=0;

    /* Encontrar los divisores */
    for (int pd=num/2; pd>1; pd--)
        if (num % pd == 0)
            div[n_div++] = pd;

    /* Imprimir el resultado */
    for (int i=0; i<n_div; i++)
        printf("%i ", div[i]);

    printf ("\n");

    for (int pd=num1/2; pd>1; pd--)
        if (num1 % pd == 0)
            div1[n_div1++] = pd;

    /* Imprimir el resultado */
    for (int i=0; i<n_div1; i++)
        printf("%i ", div1[i]);

    printf("\n");


    return EXIT_SUCCESS;
}

