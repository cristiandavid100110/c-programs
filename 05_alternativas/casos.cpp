#include <stdio.h>
#include <stdlib.h>

#define ROJO 4
#define AMARILLO 2
#define AZUL 1

int main(){
    char respuesta;
    int color = 0;

    printf("¿ves rojo? (s/n): ");
    scanf(" %c", &respuesta);
    if (respuesta == 's')
        color |= ROJO;

    printf("¿ves amarillo? (s/n): ");
    scanf(" %c", &respuesta);
    if (respuesta == 's')
        color |= AMARILLO;

   printf("¿ves azul? (s/n): "); 
   scanf(" %c", &respuesta);
   if (respuesta == 's')
       color |= AZUL;

   switch(color){
       case 0:
           printf("negro \n");
           break;
      case 1:
           printf("azul \n");
           break;
      case 2:
           printf("amarillo \n");
           break;
      case 3:
           printf("verde\n");
           break;
      case 4:
           printf("rojo\n");
           break;
      case 5:
           printf("morado\n");
           break;
       case 6:
           printf("naranja\n");
           break;
       case 7:
           printf("Blanco\n");
           break;

   }


	return EXIT_SUCCESS;
}
