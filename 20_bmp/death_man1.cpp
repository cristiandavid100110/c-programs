#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <stdio_ext.h>

/*Colores*/
#define ROJO "\x1B[31m"
#define VERDE "\x1B[32m"
#define AZUL "\x1B[34m"
#define NEGRO "\x1B[30m"
#define BLANCO "\x1B[37m"
#define MAGENTA "\x1B[35m"
#define CYAN "\x1B[36m"
#define AMARILLO "\x1B[33m"
#define RESET "\x1B[0m"

#define LMUERTE 10
#define MAX 0x100

#define DCO "diccionario.txt"

bool es_mala(char letra){
    return true;
}
int main(int argc, char *argv[]){
    char letra;
    int total_malas = 0, total_letras = 0;
    char *malas, *letras;
    FILE *pf;

    if (!(pf = fopen (DCO, "rb"))){
        fprintf(stderr,"Diccionario necesario.\n");
        return EXIT_FAILURE;
    }

    malas = (char *) malloc(LMUERTE + 1);
    letras = (char *) malloc(MAX + 1);
    bzero (malas, LMUERTE + 1);
    bzero (letras, MAX + 1);

    while(total_malas < LMUERTE){
        system("clear");
        system("toilet -f pagga AHOGADO");

        printf("Letras: " ROJO "%s" RESET "\n", letras);
        printf("Malas: " AZUL  "%s" RESET "\n", malas);
        printf("============================\n\n");

        printf("Letra: " CYAN);
        letra = getchar();
        __fpurge(stdin);
        printf(RESET"\n");
        if(strchr (letras, letra))
            continue;

        if (es_mala (letra))
            *(malas + total_malas++) = letra;
        *(letras + total_letras++) = letra;
    }

    free(malas);
    free(letras);

    return EXIT_SUCCESS;
}
