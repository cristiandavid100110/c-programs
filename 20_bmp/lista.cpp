#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 0x100

int main(int argc, char *argv[]){
    /*Zona Variables*/
    char **lista;                                   //  Array de Arrays
    char buffer[N];                                 //  Tamaño máximo
    int len;                                        //  Tamaño de la palabra

    /*Pedida de datos lista[0]*/
    printf("Dime tu nombre: ");
    scanf(" %s", buffer);

    /*Tamaño buffer y reserva de memoria*/
    len = strlen(buffer);                           //  Tamaño.
    lista = (char **) malloc (3 *sizeof (char *));  //  Reserva de memoria.
    lista[0] = (char *) malloc(len + 1);            //  Posicionamiento.
    strncpy(lista[0], buffer, len + 1);             //  Copiar lo que hay en buffer a lista.

    /*Pedida de datos lista[1]*/
    printf("Dime tu nombre: ");
    scanf(" %s", buffer);
    len = strlen (buffer);
    lista[1] = (char *) malloc(len + 1);
    strncpy (lista[1], buffer, len + 1);
    lista[2] = NULL;

    for(char **palabra = lista; *palabra != NULL; palabra++)
        printf("%s\n", *palabra);

    /*Liberar los malloc*/
    free (lista[0]);
    free (lista[1]);
    free (lista);

    return EXIT_SUCCESS;
}
