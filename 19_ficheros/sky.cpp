#include <stdio.h>
#include <stdlib.h>
/*creep contiene la direccion de momeria del primer salto de linea*/
/*el salto de linea es una constante*/

#define NOMBRE "cancion.txt"
const char * creep = "\n\
                When you were here before\n\
                Couldn't look you in the eye\n\
                You're just like an angel\n\
                Your skin makes me cry\n\
\n\
                You float like a feather\n\
                In a beautiful world\n\
                And I wish I was special\n\
                You're so fuckin' special\n\
\n\
                But I'm a creep, I'm a weirdo.\n\
                What the hell am I doing here?\n\
                I don't belong here.\n\
\n\
                I don't care if it hurts\n\
                I want to have control\n\
                I want a perfect body\n\
                I want a perfect soul\n\
\n\
                I want you to notice\n\
                When I'm not around\n\
                You're so fuckin' special\n\
                I wish I was special\n\
                But I'm a creep, I'm  weirdo.\n\
                What the hell am I doing here?\n\
                I don't belong here.\n\
                She's running…\n\
\n\
";

void print_usage(){
    printf("Esto se usa asi\n");
}

void informo(const char *mssg){
    print_usage();
    fprintf(stderr, "%s\n", mssg);
    exit (1);
}
int main(int argc, char *argv[]){
    FILE *fichero;

    if ( !(fichero = fopen ( NOMBRE, "w" )))
        informo ("No se ha podido abrir el fichero.");
    fprintf(fichero, "%s", creep);
    getchar ();
    fclose (fichero);

	return EXIT_SUCCESS;
}
