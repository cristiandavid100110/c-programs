#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define FILENAME "cancion.txt"

int main(int argc, char *argv[]){
    FILE *pf;
    int inicio, fin, distancia;

    if(! (pf = fopen(FILENAME, "r")) ){
        fprintf(stderr, "No he podido encontrar.\n");
        return EXIT_FAILURE;
    }

    inicio = ftell (pf);
    fseek (pf, 0, SEEK_END);
    fin = ftell (pf);
    distancia = fin - inicio;

    printf("Inicio: %i\n"
            "Fin: %i\n"
            "distancia: %i\n",
            inicio, fin, distancia);

    fclose(pf);

    return EXIT_SUCCESS;
}
