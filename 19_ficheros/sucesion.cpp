#include <stdio.h>
#include <stdlib.h>

#define DUMP "fibonacci.dat"

//escribir en una matriz la sucesion fibonacci, volcarlo en un fichero binario con un fwrite

int main(int argc, char *argv[]){

    int fb [] = {1, 1, 2, 3, 5, 8, 13, 21, 34, 55};
    FILE *pf;
    int n = sizeof(fb) / sizeof(int);

    if (! (pf = fopen (DUMP, "wb"))) {
        fprintf (stderr, "No he podido abrir.\n");
        return EXIT_FAILURE;
    }

    fwrite(fb, sizeof(int), n, pf);

    fclose(pf);

    return EXIT_SUCCESS;
}
