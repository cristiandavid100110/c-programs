#include <stdio.h>
#include <stdlib.h>

#define MAX 0x100
#define N 10
#define OUT "inicio.txt"
#define EXIT "final.txt"

int main(int argc, char *argv[]){
    FILE *pf;
    char nombre[N][MAX];

    if ( !(pf = fopen ( OUT, "w" )) ){
        fprintf (stderr, "No he podido abrir %s.\n", OUT);
        return EXIT_FAILURE;
    }

    printf("Dime los %i nombres: \n", N);

    for (int i=0; i<N; i++){
        printf("nombre: ");
        fgets (nombre[i], MAX, stdin);
        fprintf(pf, "%s", nombre[i]);
    }

    fclose(pf);

    return EXIT_SUCCESS;
}
