#include <stdio.h>
#include <stdlib.h>

#define MAX 0x100

struct TiEmpleado{
    char nombre[MAX];
    double sueldo;
};

int main(int argc, char *argv[]){

    struct  TiEmpleado registro;
    printf("Nombre: ");
    scanf("%s", registro.nombre);
    printf("Sueldo: ");
    scanf("%lf", &registro.sueldo);
    printf("Hola %s, Tu sueldo: %2.lf\n",registro.nombre, registro.sueldo);

    return EXIT_SUCCESS;
}
