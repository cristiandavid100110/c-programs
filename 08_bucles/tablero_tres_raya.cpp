#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ncurses.h>

/*Constantes*/
#define DELAY  150000
#define DELAY1 100000
#define N 20
#define D 3

int main(){

    initscr();


    int i, j;
    double M[D][D];

    char letra[D][D];

    system("toilet -fpagga Tres en raya");

    for (int c=1; c<N; c++){
        for(int f=1; f<N; f++)
            if (
                    c==7  || f==7  ||
                    c==13 || f==13
               )
                printw(" *");
            else
                printw("  ");
        usleep(DELAY1);
        printw("\n");
    }

    //M[i][j] = 0;
    for (int i=0; i<D; i++){
        for(int j=0; j<D; j++)
            printw("%.2lf ", M[i][j]);
        printw("\n");
    }
    printw("\n");

    printw("Fila: ");
    scanw(" %i", &i);
    printw("Columna: ");
    scanw(" %i", &j);


    printw("fin del tablero \n");
    refresh();
    getch();
    endwin();

    return EXIT_SUCCESS;
}
