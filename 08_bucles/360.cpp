#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define GRADOS
#ifdef GRADOS
#define K 1
const char *unidades = "º";
#else
#define K M_PI / 180
const char *unidades = " rad"
#endif

#define N 90

int main(){
    double grados, resultado;

    for(double grados=0; grados<K*360.; grados+=K*.5)
    printf("%.2lf%s\n", grados, unidades);

    printf("Dime los grados: ");
    scanf(" %lf", &grados);
    resultado = cos (grados * M_PI / 180 );
    printf("Coseno: de %.2lf es %lf\n",grados, resultado);

    return EXIT_SUCCESS;
}
