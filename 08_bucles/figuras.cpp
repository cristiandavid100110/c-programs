#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define AZUL "\x1B[34m"
#define ROJO "\x1B[31m"
#define NORM "\x1B[39m"
#define AMAR "\x1B[33m"
#define MAGE "\x1B[35m"

#define T 150000

int main(){

    system("clear");

    int N, L, M, K;

    printf("Número: ");
    scanf(" %i", &N);

    printf("\n");

    printf("Cuadrado\n\n");
    for (int f=1; f<=N; f++){
        for (int c=1; c<=N; c++)
                if(f==1 || c==1 || f==N || c==N)
                    printf(AZUL " □");
                else
                    printf("  ");
        usleep(T);
    printf("\n");
    }

    printf("\n");

    printf(NORM "Triángulo\n\n");
    for (int f=1; f<=N; f++){
        for (int c=1; c<=N; c++)
            if(f==N || c==1 || f==c)
                printf(ROJO " △");
            else
                printf("  ");
        usleep(T);
        printf("\n");
    }
    printf("\n");

    printf(NORM "X\n\n");
    for (int f=0; f<=N; f++){
        for (int c=0; c<=N; c++)
            if(c==f || f+c==N)
                printf(AMAR " ⁂");
            else
                printf("  ");
        usleep(T);
        printf("\n");
    }
    printf("\n");

    L = N/1.0;
    M = N/2.0;
    K = M/2.0;

    printf(NORM "Cruz\n\n");
    for (int f=0; f<=N; f++){
        for (int c=0; c<=N; c++)
            if(/*f==M ||*/ c==M || f==K)
               printf(MAGE " ♱");
            else
                printf("  ");
        usleep(T);
        printf("\n");
    }
    printf("\n");


	return EXIT_SUCCESS;
}
