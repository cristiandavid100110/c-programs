#include <stdio.h>
#include <stdlib.h>

int main(){
    char respuesta;

    do {
        printf("Quieres seguir en el programa?? (s/n): ");
        scanf(" %c", &respuesta);
        if (respuesta != 'n')
            printf("Te deseo que pases un buen día.\n");
    } while(respuesta != 'n');

    printf("Gracias por usar este programa.\n");
    return EXIT_SUCCESS;
}
