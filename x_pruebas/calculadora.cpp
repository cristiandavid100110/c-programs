#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define SUM 1
#define RES 2
#define MUL 3
#define DIV 4

int main(){
	unsigned opcion;
	double op1, op2, resultado;

	system("clear");
	system("toilet --metal -fpagga calculadora");

	printf("Elige una opción"
			"\n"
			"\t1 Suma.\n "
			"\t2 Resta.\n"
			"\t3 Multiplicación.\n"
			"\t4 División\n"
			"\n"
			"\tOpcion: "
	      );
	scanf(" %u", &opcion);

	switch(opcion){
		case SUM:
			printf("Dime el primer operando: ");
			scanf(" %lf", &op1);
			printf("Dime el segundo operando: ");
			scanf(" %lf", &op2);

			resultado = op1 + op2;
			printf("La suma es: %4.2lf\n", resultado);

			break;

		case RES:
			printf("Dime el primer operando: ");
			scanf(" %lf", &op1);
			printf("Dime el segundo operando: ");
			scanf(" %lf", &op2);

			resultado = op1 - op2;
			printf("La resta es: %4.2lf\n", resultado);

			break;

		case MUL:
			printf("Dime el primer operando: ");
			scanf(" %lf", &op1);
			printf("Dime el segundo operando: ");
			scanf(" %lf", &op2);

			resultado = op1 * op2;
			printf("La multiplicación es: %4.2lf\n", resultado);

			break;

		case DIV:
			printf("Dime el primer operando: ");
			scanf(" %lf", &op1);
			printf("Dime el segundo operando: ");
			scanf(" %lf", &op2);

			resultado = op1 / op2;
			printf("La división es: %4.2lf\n", resultado);

			break;
		default:
			printf("No sé cual es esta opción \n");
			return EXIT_FAILURE;
			
			break;
	}
	return EXIT_SUCCESS;
}
