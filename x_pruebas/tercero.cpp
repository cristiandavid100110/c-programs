#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define MENSAJE "Mis figuras chulas"

#define TRI 1
#define CUA 2
#define REC 3
#define ROM 4
#define CIR 5

void ver_menu () {

	printf("\n"
		"Elige una figura: \n"
		"\n"
		"\t1. Triángulo.\n"
		"\t2. Cuadrado.\n"
		"\t3. Rectángulo.\n"
		"\t4. Círculo.\n"
		"\n"
		"\tOpción: "
	      );
}

void pon_titulo(){

	system("toilet --metal -fpagga AREAS");
	printf("%s\n", MENSAJE);
}

int main(){

	unsigned opcion;
	double area,
	       perimetro,
	       base,
	       altura,
	       lado,
	       radio;

	system("clear");

	pon_titulo();
	ver_menu ();

	scanf(" %u", &opcion);

	switch(opcion){
		case TRI:
			printf("△ ");

			printf("Base: ");
			scanf(" %lf", &base);
			printf("Altura: ");
			scanf(" %lf", &altura);
			printf("Diagonal: ");
			scanf("%lf", &lado);

			area = (base * altura) / 2;
			perimetro = base + altura + lado;

			printf( "Area: %8.2lf\n"
				"Perímetro: %8.2lf\n",
				area, perimetro );

			break;

		case CUA:
			printf("□ ");

			printf("Lado: ");
			scanf(" %lf", &lado);

			area = lado * lado;
			perimetro = 4 * lado;

			printf( "Area: %8.2lf\n"
				"Perimetro: %8.2lf\n",
				area, perimetro );
			break;

		case REC:
			printf("▭ ");

			printf("Base: ");
			scanf("%lf", &base);
			printf("Altura: ");
			scanf("%lf", &altura);

			area = base * altura;
			perimetro = (base + altura) * 2;

			printf( "Area: %8.2lf\n"
				"Perimetro: %8.2lf\n",
				area, perimetro );
			break;

		case CIR:
			printf("○ ");

			printf("Radio: ");
			scanf("%lf", &radio);

			area = pow( radio, 2) * 3.14;
			perimetro = 2 * 3.14 * radio;

			printf( "Area: %8.2lf\n"
				"Perimetro: %8.2lf\n",
				area, perimetro );
			break;

		default:
			printf("Del 1 al 5 \n");
			return EXIT_FAILURE;
			break;
	}

	printf("\n");

	return EXIT_SUCCESS;
}
