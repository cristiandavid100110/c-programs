#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include <unistd.h>

/*Retardo*/
#define DELAY  50000
#define DELAY1 100000
#define DELAY2 150000
#define DELAY3 200000

/*Tamaño ventana*/

int main(){
    int x = 0;
    int y = 0;

    initscr();
    noecho();
    curs_set(FALSE);

    while(1){
        clear();
        mvprintw(x, y, "x");
        refresh();
        usleep(DELAY2);
        x++;
        y++;
    }
    refresh();
    getch();
    endwin();

    return EXIT_SUCCESS;
}
