#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ncurses.h>
#include <curses.h>

/*Paracompilar este archivo.*/
/*gcc texto.cpp -o texto -lncurses*/

int main(){

	initscr();

	move (6,15);
		attron(A_BOLD | A_UNDERLINE);
		printw("Morado");
		attroff(A_BOLD | A_UNDERLINE);
		refresh();
		sleep(2);
	move (8, 15);
		printw("Vamos a charlar un rato...");
		refresh();
		sleep(2);

	move (9, 15);
		printw("Aveces pienso en rojo...");
		refresh();
		sleep(2);
	move (10, 15);
		printw("Otras veces me gusta pensar en azul...");
		refresh();
		sleep(2);
	move (11, 15);
		printw("Al final del día pienso en morado.");
		refresh();
		sleep(2);
	move (12, 15);
		printw("xD");
		refresh();
		sleep(2);
	move (13, 15);
		printw("xD");
		refresh();
		sleep(2);
	move(14, 15);
		attron(A_STANDOUT);
		printw("xD");
		attroff(A_STANDOUT);
		refresh();
		sleep(2);

	refresh();
	getch();
	endwin();

	return EXIT_SUCCESS;
}
