#include <stdio.h>
#include <stdlib.h>

int main (){
	/*Ejemplo extraído de un libro*/
	double bruto,
	       impuesto,
	       neto;

	printf("Salario bruto: ");
	scanf("%lf", &bruto);
	impuesto = 0.14 * bruto;
	neto =  bruto - impuesto;
	printf("Impuestos: %2lf\n", impuesto);
	printf("Salario neto: %2lf\n", neto);

	return EXIT_SUCCESS;
}
