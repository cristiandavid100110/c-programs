#include <stdio.h>
#include <stdlib.h>

#define X 0
#define Y 1

#define D 2
#define F 4

#define G 4
#define H 3

#define DIM 2

int main(){
    double resultado;
    double op1, op2;
    double a[D][F] = {
        {4,5,3,2},
        {3,1,3,5}
    };

    double b[G][H] = {
        {1,3,4},
        {3,2,2},
        {5,1,1},
        {4,4,3}
    };

    for (int i=0; i<2; i++){
        printf("fila/columna A: ");
        scanf(" %lf,%lf", &a[i][X], &b[i][Y]);
    }

    for (int i=0; i<2; i++){
        printf("fila/columna B: ");
        scanf(" %lf,%lf", &b[i][X], &b[i][Y]);
    }

    printf("%2lf\n", a[1][3]);/*Esto debería de ser 5*/

    resultado = a[0][1] * b[2][0];/*Debería ser 25*/
    printf("%2lf\n", resultado);


	return EXIT_SUCCESS;
}
