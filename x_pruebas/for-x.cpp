#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define DELAY 150000

int main(){

    system("clear");
    system("toilet -fpagga --gay Tablero\n\n");

    printf("Inicio del tablero \n\n");
    for (int c=0; c<=20; c++){              	/*Filas*/
        for(int f=0; f<=20; f++)            	/*Columnas*/
            if (
                    c==0 || c==20 ||        /*Fila izq-der*/
                    f==0 || f==20 ||        /*Columna izq-der*/
                    c==f || c+f == 20 ||    /*Diagonales*/
                    c==10 || f==10 ||       /*Fila y columna central*/
                    c==5 || f==5 ||         /*Cuadrados izq*/
                    c==15 || f==15          /*Cuadrados Der*/
               )
                printf(" *");
            else
                printf("  ");
        usleep(DELAY);
        printf("\n");
    }

    printf("\n");
    printf(" fin del tablero \n");

    return EXIT_SUCCESS;
}
