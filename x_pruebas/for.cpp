#include <stdio.h>
#include <stdlib.h>

#define VALOR 5

int main(){
	for (int i=0; i<5; i++){
		printf(" %4i primero \n", i);
	}
	printf("\n");

	for (int i=0; i<VALOR; i++){
		printf(" %i segundo \n", i);
	}
	printf("\n");

	for (int i=0; i<VALOR; i++){
		printf(" %i tercero \t", i);
	}
	printf("\n");

	for (int i=0; i<5; i++){
		printf(" %i cuarto \v", i);
	}
	printf("\n");

	printf("fin de los bucles\n");

	return EXIT_SUCCESS;
}
