#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define DELAY 100000
#define ROJO "\x1B[31m"
#define NORM "\x1B[39m"
int main(){

	/*Forma cutre de hacer una carga, no tener encuenta*/
	//	for (int num=0; num<=100; num++){
	//		printf("Cargando:%i\n ",num);
	//		usleep(DELAY);
	//		system("clear");
	//		}

	system("clear");
	system("toilet -fpagga --metal MI BARRA");
	printf("Primera carga\n");
	for (int num=0; num<=100; num++){
		fprintf(stderr, "Cargando:%i%%\r", num);
		usleep(DELAY);
	}
	printf("\n");
	printf("He terminado la carga\n");

	printf("\n");

	printf("Segunda carga\n");
	for (int i=0; i<=100; i++){
		fprintf(stderr, ROJO "▊");
		usleep(DELAY);
	}
	printf("\n");
	printf(NORM "He terminado la carga\n");

	printf("\n");

	printf("Tercera carga\n");	
	for (int veces=0; veces<=100; veces++){
		for (int col=0; col<=veces; col++)
			fprintf(stderr, "▊");
		fprintf(stderr, "> %i%%\r", veces);
		usleep(DELAY);
	}
	printf("\n");
	printf("He terminado la carga\n");
	printf("\n");

	return EXIT_SUCCESS;
}
