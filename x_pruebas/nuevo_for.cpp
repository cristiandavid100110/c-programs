#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/* Constantes */
#define x 4
#define C 1
#define F 1 
#define N 10
#define M 5
#define DELAY 150000
#define ROJO "\x1B[31m"
#define RESET "\x1B[m"

/*Esto creo que son constantes con valor del 0 al numero de figuras*/
enum {
	cuadrado,
	triangulo,
	rectangulo,
	FIGURAS
};

/*Funciones*/
void llama_menu(){
	printf(
			"\n"
			"\tLista de figuras(%i)\n"
			"\t1. Cuadrado. \n"
			"\t2. Triángulo. \n"
			"\t3. Círculo. \n"
			"\n"
			"\tOpcion: ",
			FIGURAS
	      );
}

void llama_titulo_menu(){
	system("clear");
	system("toilet -fpagga --metal Figuras-Dibujo\n\n");
}

int main(){

	/* Variables */
	unsigned opcion;
	double op1, op2, resultado;

	/* llamada de funciones */
	llama_titulo_menu();
	llama_menu();

	/* Pedir datos de menu-->opcion */
	scanf(" %i", &opcion);

	switch(--opcion){
		case cuadrado:

			/* Limpiar pantalla/título */
			system("clear");
			system("toilet -fpagga --metal Cuadrado");

			/* Pedida de datos */
			printf("Lado: ");
			scanf(" %lf", &op1);
			resultado = op1 * x;

			printf("\n");

			/* Dibujar figura */
			printf("Cuadrado \n\n");
			for(int c=C; c<=op1; c++ ){
				for(int f=F; f<=op1; f++)
					if(
							c == C || c == op1 ||
							f == F || f == op1

					  )
						printf(ROJO " *");
					else
						printf( "  ");
				usleep(DELAY);
				printf(RESET "\n");
			}

			printf("\n");
			break;

		case triangulo:

			/* Limpiar pantalla / título */
			system("clear");
			system("toilet -fpagga --metal Triángulo");

			/* Pedida de datos */
			printf("Altura: ");
			scanf(" %lf", &op1);
			printf("Base: ");
			scanf(" %lf", &op2);
			resultado = op1 * op2 / 2;

			printf("\n");

			/* Dibujar figura */
			printf(" Triangulo rectángulo \n\n");
			for (int c=C; c<=op1; c++){             
				for (int f=F; f<=op2; f++)      
					if (
							f==F  || c==op1 || c+f==op1
					   )
						printf(ROJO " *");
					else 
						printf("  ");
				usleep(DELAY);

				printf(RESET "\n");
			}

			printf("\n");

			break;
			/*

			   printf(" rectángulo \n\n");

			   for(int c=0; c<=N; c++ ){
			   for(int f=0; f<=M; f++)
			   if(
			   c == 0 || c == 10 ||
			   f == 0 || f == 5

			   )
			   printf(" *");
			   else
			   printf("  ");
			   usleep(DELAY);

			   printf("\n");

			   }

*/

	}

	printf("El area es: %4.2lf \n", resultado);

	return EXIT_SUCCESS;
}
