#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define E 0.0001
/*Calcular numero E*/

double fc(int base, int prof){
    if (prof == 1)
        return base;
    return base +1. / fc (base, prof -1);
}

int main(int argc, char *argv[]){

    int base, i;
    double res0 = 0, res1 = -100;

    printf("Fraccion continua de numero : ");
    scanf(" %i", &base);
    printf("Profundidad: ");
    scanf(" %i", &i);

    for(int i=1; fabs (res1 - res0)>E && i<20; i++){
        res0 = res1;
        res1 = fc (base, i);
    }

    printf("resultado fraccion continua (%i, %i) = %lf\n ", base, i, res1);

	return EXIT_SUCCESS;
}
