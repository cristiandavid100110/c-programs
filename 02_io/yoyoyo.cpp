/*Pequeño repaso Txema*/
#include <stdio.h>
#include <stdlib.h>

#define VECES 10
int main(){

	printf("texto de relleno\n");

	for (int i=0; i<10; i++){
		printf("%2i yo\n", i);
	}

	for (int i=0; i<VECES; i++){
		printf("%i otro yo\n", i);
	}

	return EXIT_SUCCESS;
}
