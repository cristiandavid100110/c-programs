#include <stdio.h>
#include <stdlib.h>

int main(){

    /*Arbol gramatical*/
    /*Expresion b=2*a=3+5*/

    int a = 3 + 5;
    int b = 2 * a;
    int c = 210 / 7 * 3;

    printf(" %i \n", a);
    printf(" %i \n", b);
    printf(" %i \n", c);
    // 1 - Todos los op tienen valor de retorno
    // 2 - Los operadores tienen prioridad 
	return EXIT_SUCCESS;
}
